/**
 * Facilitates dispensing stamps for a postage stamp machine.
 */
public class StampDispenser
{
	int[] stamp;			 // Stamp denominations
	private int numOfStamps; // Number of Stamps
	
	/**
	 * Constructs a new StampDispenser that will be able to dispense the given
	 * types of stamps.
	 * 
	 * @param stampDenominations
	 *        The values of the types of stamps that the machine should
	 *        have. Should be sorted in descending order and contain at
	 *        least a 1.
	 */
	public StampDispenser(int[] stampDenominations)
	{
		if (stampDenominations == null || stampDenominations.length == 0)
		{
			int[] oneArr = { 1 };
			stamp = oneArr;
			numOfStamps = stamp.length;
			return;
		}
		bubbleSort(stampDenominations);
		stamp = stampDenominations;
		numOfStamps = stamp.length;
	}
	private void bubbleSort(int[] arr)	// Typical bubble sort
	{
		boolean swapped = false;
		do
		{
			swapped = false;
			for( int i = 0; i < arr.length-1; i++)
			{
				if(arr[i] < arr[i + 1])
				{
					int tmp = arr[i];
					arr[i] = arr[i + 1];
					arr[i + 1] = tmp;
					swapped = true;
				}
			}
		} while(swapped);
	}
	/**
	 * Returns the minimum number of stamps that the machine can dispense to
	 * fill the given request.
	 * 
	 * Using dynamic programming to get the value of the minimum stamps necessary.
	 * 
	 * @param request
	 *        The total value of the stamps to be dispensed.
	 */
	public int calcMinNumStampsToFillRequest(int request)
	{
		int currentStamp = 0;		     // Initialize amount of Stamps needed
		int m = numOfStamps + 1;    
		int n = request + 1;
		int inf = Integer.MAX_VALUE - 1; // Fake infinity :P
		
		// Create array & fill with infinity
		int[][] stampTable = new int[m][n];  
		for( int j = 1; j < n; j++)
			stampTable[0][j] = inf;
		
		for(int stampPosition = 1; stampPosition < m; stampPosition++)
		{
			for(int stampValue = 1; stampValue < n; stampValue++)
			{
				if(stampValue - stamp[stampPosition - 1] >= 0)
				{
					currentStamp = 
							stampTable[stampPosition]
									[stampValue - stamp[stampPosition - 1]];
				}
				else
					currentStamp = inf;
				stampTable[stampPosition][stampValue] = 
					Math.min(stampTable[stampPosition - 1][stampValue],
														1 + currentStamp);
			}
		}
		return stampTable[m-1][n-1];
	}


	public static void main(String[] args)
	{
		int[] denominations = { 90, 30, 24, 10, 6, 2, 1 };
		StampDispenser stampDispenser = new StampDispenser(denominations);
		for(int i = 0; i < stampDispenser.stamp.length; i++)
			System.out.println("["+i+"]: " + stampDispenser.stamp[i]);
		assert stampDispenser.calcMinNumStampsToFillRequest(18) == 3;
	}
}